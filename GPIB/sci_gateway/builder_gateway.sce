// Copyright 2015-2016 Scilab Enterprises
// This work is licensed under a Creative Commons Attribution-NoDerivatives 4.0 International License.
// http://creativecommons.org/licenses/by-nd/4.0/legalcode

function builder_gateway()

    sci_gateway_dir = get_absolute_file_path("builder_gateway.sce");
    languages       = ["c"];

    tbx_builder_gateway_lang(languages,sci_gateway_dir);
    tbx_build_gateway_loader(languages,sci_gateway_dir);
    tbx_build_gateway_clean(languages,sci_gateway_dir);

endfunction

builder_gateway()
clear builder_gateway;
