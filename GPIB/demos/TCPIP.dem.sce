// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab␊
// Copyright (C) 2014 - Scilab Enterprises - Sylvain GENIN
//
// Copyright 2015-2016 Scilab Enterprises
// This work is licensed under a Creative Commons Attribution-NoDerivatives 4.0 International License.
// http://creativecommons.org/licenses/by-nd/4.0/legalcode
//

function TCPIP_VISA()

    mode(-1);
    lines(0);

    orig_format = format();
    format(25);

    disp("First we will need to open the default resource manager.");
    disp("[status, defaultRM] = viOpenDefaultRM()");

    [status, defaultRM] = viOpenDefaultRM();
    if status < viGetDefinition("VI_SUCCESS") then
    error("Could not open a session to the VISA Resource Manager!");
    end
    disp(defaultRM,"defaultRM =");

    disp("[status, instr] = viOpen(defaultRM, ""TCPIP0::ftp.ni.com::21::SOCKET"", viGetDefinition(""VI_NULL""),viGetDefinition(""VI_NULL""));");
    [status, instr] = viOpen(defaultRM, "TCPIP0::ftp.ni.com::21::SOCKET", viGetDefinition("VI_NULL"),viGetDefinition("VI_NULL"));
    if status < viGetDefinition("VI_SUCCESS") then
    viClose (defaultRM);
    error("An error occurred opening the session to TCPIP0::ftp.ni.com::21::SOCKET");
    end

 disp("viSetAttribute(instr, viGetDefinition(""VI_ATTR_TCPIP_NODELAY""), viGetDefinition(""VI_TRUE""));");
    viSetAttribute(instr, viGetDefinition("VI_ATTR_TCPIP_NODELAY"), viGetDefinition("VI_TRUE"));


 disp("[status, outputBuffer, nbCa] = viRead(instr, 25);");
   [status, outputBuffer, nbCa] = viRead(instr, 25);
    if status < viGetDefinition("VI_SUCCESS") then
    viClose (defaultRM);
    error(status,"viRead failed with error code");
    end

    disp("Server returned :");
    disp(outputBuffer);
    disp("Server port is read by viGetDefinition function, which needs a pointer to a UINT16");
    disp("pPortNo = new_ViPUInt16();");
    pPortNo = new_ViPUInt16();
    disp("The pointer is passed to the viGetAttribute function.");
    disp("status = viGetAttribute(instr, viGetDefinition(""VI_ATTR_TCPIP_PORT""), pPortNo);");
    status = viGetAttribute(instr, viGetDefinition("VI_ATTR_TCPIP_PORT"), pPortNo);
    disp("Port:");
    disp("ViPUInt16_value function is used to read the value of the pointer UINT16 .");
    disp("DataPortNo = ViPUInt16_value(pPortNo);");
    portNo = ViPUInt16_value(pPortNo);
    disp(portNo);


    disp("Closing session");
    disp("viClose(instr);");
    disp("viClose(defaultRM);");

    viClose(instr);
    viClose(defaultRM);

    format(orig_format(2));

endfunction

TCPIP_VISA();
clear TCPIP_VISA; 
