%apply int *OUTPUT { ViPSession vi };

%apply char *OUTPUT { ViChar _VI_FAR desc[] };
%apply char *OUTPUT { ViChar _VI_FAR rsrcClass[] };
%apply char *OUTPUT { ViChar _VI_FAR expandedUnaliasedName[] };
%apply char *OUTPUT { ViChar _VI_FAR aliasIfExists[] };
%apply char *OUTPUT { ViChar _VI_FAR accessKey[] };

%apply unsigned char *OUTPUT { ViPUInt8 val8 };
%apply unsigned char *OUTPUT { ViAUInt8 buf8 };

%apply unsigned short *OUTPUT { ViPUInt16 val16 };
%apply unsigned short *OUTPUT { ViAUInt16 buf16 };
%apply unsigned short *OUTPUT { ViPUInt16 intfNum };
%apply unsigned short *OUTPUT { ViPUInt16 intfType };
%apply unsigned short *OUTPUT { ViPUInt16 status };
%apply unsigned short *OUTPUT { ViAInt16 trigBuses };
%apply unsigned short *OUTPUT { ViAInt16 trigLines };

%apply short *OUTPUT { ViPInt16 failureIndex };

%apply unsigned int *OUTPUT { ViPUInt32 val32 };
%apply unsigned int *OUTPUT { ViPUInt32 response};
%apply unsigned int *OUTPUT { ViAUInt32 buf32 };

%apply ViUInt32 *OUTPUT { ViPUInt32 retCnt };
%apply ViUInt32 *OUTPUT { ViPEventType outEventType };
%apply ViUInt32 *OUTPUT { ViPEvent outContext };
%apply ViUInt32 *OUTPUT { ViPFindList vi };
%apply ViUInt32 *OUTPUT { ViPJobId jobId };

%apply unsigned char *OUTPUT { ViPBuf buf };

#if defined(_VI_INT64_UINT64_DEFINED) && defined(_VISA_ENV_IS_64_BIT)
%apply ViUInt32 *OUTPUT { ViPBusAddress offset };
#else
%apply ViUInt32 *OUTPUT { ViPBusAddress offset };
#endif

#if defined(_VI_INT64_UINT64_DEFINED)
%apply ViUInt64 *OUTPUT { ViPBusAddress64 offset };
%apply ViUInt64 *OUTPUT { ViPUInt64 val64 };
%apply ViUInt64 *OUTPUT { ViAUInt64 buf64 };

#endif

%include "cpointer.i"

%pointer_functions(ViUInt32, ViPUInt32);
%pointer_functions(ViUInt32, ViAUInt32);
%pointer_functions(ViInt32, ViPInt32);
%pointer_functions(ViInt32, ViAInt32);

%pointer_functions(ViUInt16, ViPUInt16);
%pointer_functions(ViUInt16, ViAUInt16);

%pointer_functions(ViInt16, ViPInt16);
%pointer_functions(ViInt16, ViAInt16);

%pointer_functions(ViUInt8, ViPUInt8);
%pointer_functions(ViUInt8, ViAUInt8);

%pointer_functions(ViInt8, ViPInt8);
%pointer_functions(ViInt8, ViAInt8);

%pointer_functions(ViChar, ViPChar);
%pointer_functions(ViChar, ViAChar);

%pointer_functions(ViByte, ViPByte);
%pointer_functions(ViByte, ViAByte);

%pointer_functions(ViAddr, ViPAddr);
%pointer_functions(ViAddr, ViAAddr);

%pointer_functions(ViReal32, ViPReal32);
%pointer_functions(ViReal32, ViAReal32);

%pointer_functions(ViReal64, ViPReal64);
%pointer_functions(ViReal64, ViAReal64);

/*%pointer_functions(ViChar, ViString);

%pointer_functions(unsigned char , ViBuf);*/
%pointer_functions(unsigned char , ViPBuf);
/*%pointer_functions(unsigned char *, ViABuf);*/

%pointer_functions(ViBoolean, ViPBoolean);
%pointer_functions(ViBoolean, ViABoolean);

%pointer_functions(ViStatus, ViPStatus);
%pointer_functions(ViStatus, ViAStatus);

%pointer_functions(ViVersion, ViPVersion);
%pointer_functions(ViVersion, ViAVersion);

%pointer_functions(ViObject, ViPObject);
%pointer_functions(ViObject, ViAObject);

%pointer_functions(ViSession, ViASession);
