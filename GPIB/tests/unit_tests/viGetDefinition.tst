// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2014 - Scilab Enterprises - Sylvain GENIN
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

scilab_visa_Init();

assert_checkequal(viGetDefinition("_VI_ERROR"), double(_VI_ERR)) ;
assert_checkequal(viGetDefinition("VI_ATTR_ASRL_DATA_BITS"), double(VI_AR_AL_DATA_BITS));
assert_checkequal(viGetDefinition("VI_ATTR_GPIB_ADDR_STATE"), double(VI_AR_GB_ADDR_STA));
assert_checkequal(viGetDefinition("VI_GPIB_UNADDRESSED"), double(VI_GB_UNADDED));
assert_checkequal(viGetDefinition("VI_ATTR_RD_BUF_SIZE"), double(VI_AR_RD_BUF_SZ));
assert_checkequal(viGetDefinition("VI_ATTR_FIREWIRE_UPPER_CHIP_ID"), double(VI_AR_FRW_UP_CHIP_ID));
assert_checkequal(viGetDefinition("VI_PXI_STAR_TRIG_CONTROLLER"), double(VI_PXI_STAR_TRIG_CTRL));
assert_checkequal(viGetDefinition("VI_ATTR_FIREWIRE_DEST_UPPER_OFFSET"), double(VI_AR_FRW_DEST_UP_OFFSET));
