// Copyright 2015-2016 Scilab Enterprises
// This work is licensed under a Creative Commons Attribution-NoDerivatives 4.0 International License.
// http://creativecommons.org/licenses/by-nd/4.0/legalcode

tbx_builder_help_lang(["en_US"], ..
get_absolute_file_path("builder_help.sce"));
