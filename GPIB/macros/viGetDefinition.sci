// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab␊
// Copyright (C) 2014 - Scilab Enterprises - Sylvain GENIN
//
// Copyright 2015-2016 Scilab Enterprises
// This work is licensed under a Creative Commons Attribution-NoDerivatives 4.0 International License.
// http://creativecommons.org/licenses/by-nd/4.0/legalcode
//
//
function [vDefinition] = viGetDefinition(str)

scilab_visa_Init();

vDefinition = 0;
str = strsubst(str,'ERROR','ERR');
str = strsubst(str,'ATTR','AR');
str = strsubst(str,'ASRL','AL');
str = strsubst(str,'GPIB','GB');
str = strsubst(str,'UPPER','UP');
str = strsubst(str,'FIREWIRE','FRW');
str = strsubst(str,'SIZE','SZ');
str = strsubst(str,'BASE_BAR','BBAR');
str = strsubst(str,'HISLIP','HSLIP');
str = strsubst(str,'_DTR','');
str = strsubst(str,'ADDRESS','ADD');
str = strsubst(str,'SECONDARY','SCD');
str = strsubst(str,'EVENT','EVT');
str = strsubst(str,'EXCLUSIVE','EXCL');
str = strsubst(str,'CONTROLLER','CTRL');
str = strsubst(str,'VI_PXI_LBUS_STAR_TRIG_BUS_','VI_PXI_LBUS_STAR_TRIG_');
str = strsubst(str,'MESSAGE','MSG');
str = strsubst(str,'VERSION','VRS');
str = strsubst(str,'MAX','MX');
str = strsubst(str,'RIGHT','R');
str = strsubst(str,'HANDSHAKE','HANDS');
str = strsubst(str,'WRITE','WR');
str = strsubst(str,'_KB','');
str = strsubst(str,'STATUS','STS');
str = strsubst(str,'STATE','STA');
str = strsubst(str,'SYSFAIL','SYSF');
str = strsubst(str,'COMBINE','COMB');
str = strsubst(str,'OVERLAP_EN','OVERL');
str = "vDefinition = double(" + str + ");";

  ierr = execstr(str, 'errcatch');
  if ierr <> 0 then
    err_msg = lasterror();
  end

endfunction
