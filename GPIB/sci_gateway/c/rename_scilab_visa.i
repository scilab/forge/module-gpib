//%rename("************************") VI_PXI_LBUS_STAR_TRIG_BUS_0;
//%rename("VI_PXI_LBUS_BUS_0") VI_PXI_LBUS_STAR_TRIG_BUS_0;

//ERROR 				-> ERR
//ATTR  				-> AR
//ASRL  				-> AL
//GPIB					-> GB
//UPPER     				-> UP
//FIREWIRE				-> FRW
//SIZE					-> SZ
//BASE_BAR				-> BBAR
//HISLIP				-> HSLIP
//_DTR					-> 
//ADDRESS				-> ADD
//SECONDARY				-> SCD
//EVENT					-> EVT
//EXCLUSIVE				-> EXCL
//CONTROLLER				-> CTRL
//VI_PXI_LBUS_STAR_TRIG_BUS__		-> VI_PXI_LBUS_STAR_TRIG_
//MESSAGE				-> MSG
//VERSION				-> VRS
//MAX					-> MX
//RIGHT					-> R
//HANDSHAKE				-> HANDS
//WRITE					-> WR
//_KB					->
//STATUS				-> STS
//STATE					-> STA
//SYSFAIL				-> SYSF
//COMBINE				-> COMB
//OVERLAP_EN				-> OVERL

%rename("%(regex:/(.*)SYSFAIL(.*)/\\1SYSF\\2/)s") "";
%rename("%(regex:/(.*)STATE(.*)/\\1STA\\2/)s") "";
%rename("%(regex:/(.*)STATUS(.*)/\\1STS\\2/)s") "";
%rename("%(regex:/(.*)WRITE(.*)/\\1WR\\2/)s") "";
%rename("%(regex:/(.*)MAX(.*)/\\1MX\\2/)s") "";
%rename("%(regex:/(.*)VERSION(.*)/\\1VRS\\2/)s") "";
%rename("%(regex:/(.*)MESSAGE(.*)/\\1MSG\\2/)s") "";
%rename("%(regex:/(.*)FIREWIRE(.*)/\\1FRW\\2/)s") "";
%rename("%(regex:/(.*)ERROR(.*)/\\1ERR\\2/)s") "";
%rename("%(regex:/(.*)ERROR(.*)ERROR(.*)/\\1ERR\\2ERR\\3/)s") "";
%rename("%(regex:/(.*)ERROR(.*)SIZE(.*)/\\1ERR\\2SZ\\3/)s") "";

%rename("%(regex:/(.*)ATTR(.*)/\\1AR\\2/)s") "";
%rename("%(regex:/(.*)ATTR(.*)MAX(.*)/\\1AR\\2MX\\3/)s") "";
%rename("%(regex:/(.*)ATTR(.*)VERSION(.*)/\\1AR\\2VRS\\3/)s") "";
%rename("%(regex:/(.*)ATTR(.*)STATUS(.*)/\\1AR\\2STS\\3/)s") "";
%rename("%(regex:/(.*)ATTR(.*)STATE(.*)/\\1AR\\2STA\\3/)s") "";
%rename("%(regex:/(.*)ATTR(.*)FIREWIRE(.*)/\\1AR\\2FRW\\3/)s") "";
%rename("%(regex:/(.*)ATTR(.*)RIGHT(.*)/\\1AR\\2R\\3/)s") "";
%rename("%(regex:/(.*)ATTR(.*)UPPER(.*)/\\1AR\\2UP\\3/)s") "";
%rename("%(regex:/(.*)ATTR(.*)WRITE(.*)/\\1AR\\2WR\\3/)s") "";
%rename("%(regex:/(.*)ATTR(.*)WRITE(.*)COMBINE(.*)/\\1AR\\2WR\\3COMB\\4/)s") "";
%rename("%(regex:/(.*)ATTR(.*)FIREWIRE(.*)UPPER(.*)/\\1AR\\2FRW\\3UP\\4/)s") "";
%rename("%(regex:/(.*)ATTR(.*)SYSFAIL(.*)STATE(.*)/\\1AR\\2SYSF\\3STA\\4/)s") "";
%rename("%(regex:/(.*)ATTR(.*)SIZE(.*)/\\1AR\\2SZ\\3/)s") "";
%rename("%(regex:/(.*)ATTR(.*)MAX(.*)SIZE(.*)/\\1AR\\2MX\\3SZ\\4/)s") "";
%rename("%(regex:/(.*)ATTR(.*)BASE_BAR(.*)/\\1AR\\2BBAR\\3/)s") "";
%rename("%(regex:/(.*)ATTR(.*)HISLIP(.*)/\\1AR\\2HSLIP\\3/)s") "";
%rename("%(regex:/(.*)ATTR(.*)HISLIP(.*)MESSAGE(.*)/\\1AR\\2HSLIP\\3MSG\\4/)s") "";
%rename("%(regex:/(.*)ATTR(.*)HISLIP(.*)MAX(.*)MESSAGE(.*)/\\1AR\\2HSLIP\\3MX\\4MSG\\5/)s") "";
%rename("%(regex:/(.*)ATTR(.*)HISLIP(.*)MAX(.*)MESSAGE(.*)_KB/\\1AR\\2HSLIP\\3MX\\4MSG\\5/)s") "";
%rename("%(regex:/(.*)ATTR(.*)HISLIP(.*)VERSION(.*)/\\1AR\\2HSLIP\\3VRS\\4/)s") "";
%rename("%(regex:/(.*)ATTR(.*)HISLIP(.*)OVERLAP_EN(.*)/\\1AR\\2HSLIP\\3OVERL\\4/)s") "";
%rename("%(regex:/(.*)ERROR(.*)ATTR(.*)/\\1ERR\\2AR\\3/)s") "";
%rename("%(regex:/(.*)ERROR(.*)ATTR(.*)STATE(.*)/\\1ERR\\2AR\\3STA\\4/)s") "";

%rename("%(regex:/(.*)ASRL(.*)/\\1AL\\2/)s") "";
%rename("%(regex:/(.*)ASRL(.*)_DTR(.*)/\\1AL\\2\\3/)s") "";
%rename("%(regex:/(.*)ERROR(.*)ASRL(.*)/\\1ERR\\2AL\\3/)s") "";
%rename("%(regex:/(.*)ATTR(.*)ASRL(.*)/\\1AR\\2AL\\3/)s") "";
%rename("%(regex:/(.*)ATTR(.*)ASRL(.*)STATE(.*)/\\1AR\\2AL\\3STA\\4/)s") "";
%rename("%(regex:/(.*)ATTR(.*)ASRL(.*)STATE(.*)_DTR(.*)/\\1AR\\2AL\\3STA\\4\\5/)s") "";
%rename("%(regex:/(.*)ATTR(.*)ASRL(.*)_DTR(.*)STATE(.*)/\\1AR\\2AL\\3\\4STA\\5/)s") "";

%rename("%(regex:/(.*)GPIB(.*)/\\1GB\\2/)s") "";
%rename("%(regex:/(.*)GPIB(.*)ADDRESS(.*)/\\1GB\\2ADD\\3/)s") "";
%rename("%(regex:/(.*)ATTR(.*)GPIB(.*)/\\1AR\\2GB\\3/)s") "";
%rename("%(regex:/(.*)ATTR(.*)GPIB(.*)ADDRESS(.*)/\\1AR\\2GB\\3ADD\\4/)s") "";
%rename("%(regex:/(.*)ATTR(.*)GPIB(.*)SECONDARY(.*)/\\1AR\\2GB\\3SCD\\4/)s") "";
%rename("%(regex:/(.*)ATTR(.*)GPIB(.*)STATE(.*)/\\1AR\\2GB\\3STA\\4/)s") "";
%rename("%(regex:/(.*)ATTR(.*)GPIB(.*)ADDRESS(.*)STATE(.*)/\\1AR\\2GB\\3ADD\\4STA\\5/)s") "";
%rename("%(regex:/(.*)ATTR(.*)GPIB(.*)SECONDARY(.*)ADDRESS(.*)/\\1AR\\2GB\\3SCD\\4ADD\\5/)s") "";
%rename("%(regex:/(.*)GPIB(.*)ASSERT_(.*)/\\1GB\\2ASST_\\3/)s") "";
%rename("%(regex:/(.*)GPIB(.*)ASSERT_(.*)ADDRESS(.*)/\\1GB\\2ASST_\\3ADD\\4/)s") "";
%rename("%(regex:/(.*)GPIB(.*)ASSERT_(.*)HANDSHAKE(.*)/\\1GB\\2ASST_\\3HANDS\\4/)s") "";

%rename("%(regex:/(.*)EVENT(.*)/\\1EVT\\2/)s") "";
%rename("%(regex:/(.*)EVENT(.*)ASRL(.*)/\\1EVT\\2AL\\3/)s") "";
%rename("%(regex:/(.*)EVENT(.*)GPIB(.*)/\\1EVT\\2GB\\3/)s") "";
%rename("%(regex:/(.*)EVENT(.*)SYSFAIL(.*)/\\1EVT\\2SYSF\\3/)s") "";
%rename("%(regex:/(.*)ERROR(.*)EVENT(.*)/\\1ERR\\2EVT\\3/)s") "";
%rename("%(regex:/(.*)ATTR(.*)EVENT(.*)/\\1AR\\2EVT\\3/)s") "";

%rename("%(regex:/(.*)EXCLUSIVE(.*)/\\1EXCL\\2/)s") "";

%rename("%(regex:/(.*)CONTROLLER(.*)/\\1CTRL\\2/)s") "";

%rename("%(regex:/VI_PXI_LBUS_STAR_TRIG_BUS_(.*)/VI_PXI_LBUS_STAR_TRIG_\\1/)s") "";

