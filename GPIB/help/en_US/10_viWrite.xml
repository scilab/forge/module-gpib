<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab␊
 * Copyright (C) 2015 - Scilab Enterprises - Sylvain GENIN
 *
 * Copyright 2015-2016 Scilab Enterprises
 * This work is licensed under a Creative Commons Attribution-NoDerivatives 4.0 International License.
 * http://creativecommons.org/licenses/by-nd/4.0/legalcode
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns3="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="viWrite" xml:lang="en">
    <refnamediv>
        <refname>viWrite</refname>
        <refpurpose>Writes synchronously data to device or interface </refpurpose>
    </refnamediv>
    <refsynopsisdiv>
        <title>Calling Sequence</title>
        <synopsis>[status, writeCount] = viWrite(session, buf)</synopsis>
    </refsynopsisdiv>
    <refsection>
        <title>Arguments</title>
        <variablelist>
            <varlistentry>
                <term>status</term>
                <listitem>
                    <para>a real.</para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term>writeCount</term>
                <listitem>
                    <para>number of bytes actually transferred.</para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term>session</term>
                <listitem>
                    <para>unique logical identifier of a session.</para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term>buf</term>
                <listitem>
                    <para>a character string.</para>
                    <para>location of a data block to be sent to a device.</para>
                </listitem>
            </varlistentry>
        </variablelist>
    </refsection>
    <refsection>
        <title>Description</title>
        <para>The <link linkend="viWrite">viWrite()</link> operation synchronously transfers data. The data to be written is in the buffer represented by buf. This operation returns only when the transfer is terminated. Only one synchronous write operation can occur at a time.</para>
    </refsection>
    <refsection>
        <title>Examples</title>
        <programlisting role="example">
        [status, defaultRM] = viOpenDefaultRM();

        // Write a command to device located at TCPIP0::ftp.ni.com::21::SOCKET
        [status, instr] = viOpen(defaultRM, "TCPIP0::ftp.ni.com::21::SOCKET", viGetDefinition("VI_NULL"), viGetDefinition("VI_NULL"));
        [status, count] = viWrite(instr, ":AUT")

        viClose(instr);
        viClose(defaultRM);
        </programlisting>
    </refsection>
</refentry>
