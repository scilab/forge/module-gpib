<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab␊
 * Copyright (C) 2014 - Scilab Enterprises - Sylvain GENIN
 *
 * Copyright 2015-2016 Scilab Enterprises
 * This work is licensed under a Creative Commons Attribution-NoDerivatives 4.0 International License.
 * http://creativecommons.org/licenses/by-nd/4.0/legalcode
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns3="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="Quick_start" xml:lang="en">
    <refnamediv>
        <refname>Quick start</refname>
        <refpurpose>This help contains information on using the VISA Library Application Programmer’s Interface (API) under Scilab.</refpurpose>
    </refnamediv>
    <refsection>
        <title>Introduction</title>
            <para><emphasis role="bold">VISA</emphasis></para>
            <para> The Virtual Instrument Software Architecture (VISA) is a standard for configuring, programming, and troubleshooting instrumentation systems comprising GPIB, VXI, PXI, Serial, Ethernet, and/or USB interfaces.</para>
            <para> VISA provides the programming interface between the hardware and development environments.</para>
            <para><emphasis role="bold">GPIB</emphasis></para>
            <para> GPIB (General Purpose Interface Bus) comes from IEEE-488 standard. It is a short-range digital communications bus specification. It was created for use with automated test equipment.</para>

            <para><emphasis role="bold">Pre-requisites</emphasis></para>
            <simplelist type="inline">
            <member><para>Appropriate hardware, in the form of a National Instruments GPIB,GPIB-VXI, MXI/VXI or serial interface board. </para></member>
            <member><para>For GPIB applications, install NI-488. For VXI applications, install NI-VXI. For other hardware interfaces, NI-VISA uses the system’s standard drivers.</para></member>
            <member><para><ulink url="http://www.ni.com/download/ni-visa-14.0/4722/en/">NI-VISA distribution media.</ulink></para></member>
            </simplelist>

            <para><emphasis role="bold">You can use both National Instruments (GPIB) and Agilent/HP (HPIB) Controllers in the same System :</emphasis></para>
            <para> To activate the interface between NI_VISA and Agilent, you must enable the NiTulip mode of NI_VISA.
            Under Windows :</para>
            <simplelist type="inline">
            <member><para> Search "NI MAX" in order to start it.</para></member>
            <member><para> In "NI MAX" go to "System"->"Software" and select "NI-VISA".</para></member>
            <member><para> In "NI-VISA" go to "My System"->"General Settings" and select "Passports".</para></member>
            <member><para> Check NiTulip.dll.</para></member>
            </simplelist>
            <para>You can find this information <ulink url="http://digital.ni.com/public.nsf/allkb/3B3626D9C1F999218625694200791AD7">here</ulink>.</para>
    </refsection>

      <refsynopsisdiv>
    <title>Tutorial</title>
        <para><emphasis role="bold">Get the list of connected devices</emphasis></para>
        <para>To connect between your computer and your device you need the address of the instrument.</para>
        <para>For this, run the <link linkend="findAllInstruments">findAllInstruments()</link> which gives you the list of all connected instruments:
            <programlisting>[status, deviceAddrs] = findAllInstruments()</programlisting>
            <variablelist>
                <varlistentry>- <term>status</term> contains the return code of the operation.</varlistentry>
                <varlistentry>- <term>deviceAddrs</term> contains the list of descriptors (or adresses) of all connected devices.</varlistentry>
            </variablelist>
            If <link linkend="findAllInstruments">findAllInstruments()</link> hasn't found any device, it returns [].
        </para>

        <para><emphasis role="bold">Open a session</emphasis></para>
        <para><link linkend="viOpenDefaultRM">viOpenDefaultRM()</link> creates a session and returns its identifier.</para>
        <programlisting>[status, defaultRM] = viOpenDefaultRM();</programlisting>
        <variablelist>
            <varlistentry>- <term>status</term> contains the return code of the operation.</varlistentry>
            <varlistentry>- <term>defaultRM</term> is a unique logical identifier of the Default Resource Manager session.</varlistentry>
        </variablelist>

        <para><emphasis role="bold">Connect to a device</emphasis></para>
        <para>To open a communication with the first connected device:</para>
        <programlisting>[status, idDevice] = viOpen(defaultRM, deviceAddrs(1), viGetDefinition("VI_NULL"), viGetDefinition("VI_NULL"));</programlisting>
        <variablelist>
            <varlistentry>- <term>defaultRM</term> defaultRM is Resource Manager session—should always be a session returned from <link linkend="viOpenDefaultRM">viOpenDefaultRM()</link>.</varlistentry>
            <varlistentry>- <term>deviceAddrs(1)</term> is the address of the first connected device.</varlistentry>
            <varlistentry>- the first <term>viGetDefinition("VI_NULL")</term> specifies the mode by which the resource is accessed, here the session uses VISA-supplied default values (please Refer to the Description section for valid values)..</varlistentry>
            <varlistentry>- the second <term>viGetDefinition("VI_NULL")</term> specifies the maximum time period (in milliseconds) the operation waits before returning an error (this does not set the I/O timeout—to do that you must call viSetAttribute() with the attribute VI_ATTR_TMO_VALUE).</varlistentry>
            <varlistentry>- <term>status</term> contains the return code of the operation.</varlistentry>
            <varlistentry>- <term>idDevice</term> is the device identifier.</varlistentry>
        </variablelist>

        <para><emphasis role="bold">Send commands</emphasis></para>
        <para>Once connected, you can send commands to your instrument using the function <link linkend="viWrite">viWrite()</link>.</para>
        <programlisting>[status, count] = viWrite(idDevice, "*IDN?");</programlisting>
        <variablelist>
            <varlistentry>- <term>idDevice</term> is the device identifier.</varlistentry>
            <varlistentry>- <term>"*IDN?"</term> is a command to get a device identification string (a command ending with "?" will return a value). Please refer to your instrument manual to get the list of its supported instructions.</varlistentry>
            <varlistentry>- <term>status</term> contains the return code of the operation.</varlistentry>
            <varlistentry>- <term>count</term> is the number of bytes actually transferred.</varlistentry>
        </variablelist>

        <para><emphasis role="bold">Read data</emphasis></para>
        <para>The <link linkend="viRead">viRead()</link> is used for this purpose:</para>
        <programlisting>[status, bufferOut, count] = viRead(idDevice, 255);</programlisting>
        <variablelist>
            <varlistentry>- <term>idDevice</term> is the device identifier.</varlistentry>
            <varlistentry>- <term>255</term> is number of bytes to be read.</varlistentry>
            <varlistentry>- <term>status</term> contains the return code of the operation.</varlistentry>
            <varlistentry>- <term>bufferOut</term> contains the read data.</varlistentry>
            <varlistentry>- <term>count</term> is the number of bytes actually read.</varlistentry>
        </variablelist>

        <para><emphasis role="bold">Read attributes</emphasis></para>
        <para>You can also read one of the instruments attributes with the <link linkend="viGetAttribute">viGetAttribute()</link> function.</para>
        <para>This function needs a pointer on the attribute, so you need to create a pointer of the attribute type, then pass it to the function.</para>
        <para>Then, you will need to read the pointer value, use the dedicated function for this:</para>
        <programlisting>
        pQueueLength = new_ViPUInt16();
        status = viGetAttribute(idDevice, viGetDefinition("VI_ATTR_MAX_QUEUE_LENGTH"), pQueueLength);
        queueLength = ViPUInt16_value(pQueueLength);
        </programlisting>
        <variablelist>
            <varlistentry>- <term>idDevice</term> is the device identifier.</varlistentry>
            <varlistentry>- <term>viGetDefinition("VI_ATTR_MAX_QUEUE_LENGTH")</term> returns the identifier of the Max Queue Length attribute.</varlistentry>
            <varlistentry>- <term>pQueueLength</term> is the pointer to the value of the attribute.</varlistentry>
            <varlistentry>- <term>status</term> contains the return code of the operation.</varlistentry>
        </variablelist>

        <para><emphasis role="bold">Disconnect from the device</emphasis></para>
        <para>The communication with the device is over, you can close it using the function <link linkend="viClose">viClose()</link>.</para>
        <programlisting>viClose(idDevice);</programlisting>
        <variablelist>
            <varlistentry>- <term>idDevice</term> is the device identifier.</varlistentry>
        </variablelist>

        <para><emphasis role="bold">Close the session</emphasis></para>
        <para>The same command is used to close the session.</para>
        <programlisting>viClose(defaultRM);</programlisting>
        <variablelist>
            <varlistentry>- <term>defaultRM</term> is a unique logical identifier of the Default Resource Manager session.</varlistentry>
        </variablelist>
    </refsynopsisdiv>

    <refsection>
        <title>Tutorial full script</title>
        <programlisting role="example">
        [status, deviceAddrs] = findAllInstruments();

        [status, defaultRM] = viOpenDefaultRM();

        // if no device is connected, use NI FTP as device
        if deviceAddrs == [] then
            deviceAddrs = "TCPIP0::ftp.ni.com::21::SOCKET";
        end

        [status, idDevice] = viOpen(defaultRM, deviceAddrs(1), viGetDefinition("VI_NULL"),viGetDefinition("VI_NULL"));

        [status, count] = viWrite(idDevice, "*IDN?");
        [status, bufferOut, count] = viRead(idDevice, 255)

        pMaxQueueLength = new_ViPUInt16();
        status = viGetAttribute(idDevice, viGetDefinition("VI_ATTR_MAX_QUEUE_LENGTH"), pMaxQueueLength);
        maxQueueLength = ViPUInt16_value(pMaxQueueLength)
        delete_ViPUInt16(pMaxQueueLength);

        viClose(idDevice);
        viClose(defaultRM);
        </programlisting>
    </refsection>

    <refsection role="see also">
        <title>See also</title>
        <simplelist type="inline">
            <member>
                <ulink url="http://www.ni.com/pdf/manuals/370423a.pdf">NI-VISA User Manual.</ulink>
            </member>
            <member>
                <ulink url="http://www.ni.com/pdf/manuals/370132c.pdf">NI-VISA Programmer Manual.</ulink>
            </member>
        </simplelist>
    </refsection>
</refentry>
