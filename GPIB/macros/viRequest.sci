// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab␊
// Copyright (C) 2015 - Scilab Enterprises - Sylvain GENIN
//
// Copyright 2015-2016 Scilab Enterprises
// This work is licensed under a Creative Commons Attribution-NoDerivatives 4.0 International License.
// http://creativecommons.org/licenses/by-nd/4.0/legalcode
//
//
function [status, data] = viRequest(session, str)
data = "";
str = strsubst(str,"\n","");
[status, count] = viWrite(session, str);
    if status < viGetDefinition("VI_SUCCESS") then
        error("Error : write command");
        return;
    end
    
[status, data, count] = viRead(session, 255);
    if (status < viGetDefinition("VI_SUCCESS")) & ( length(data) == 0) then
        error("Error : read command");
        return;
    end

while ~(status <= viGetDefinition("VI_SUCCESS"))
[status, datatempo, count] = viRead(session, 255);
data =  strcat([data,datatempo]);
end
    
endfunction
